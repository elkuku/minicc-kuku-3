<?php
if (getenv("OPENSHIFT_APP_NAME")) {
    $container->setParameter('database_host', getenv("OPENSHIFT_MYSQL_DB_HOST"));
    $container->setParameter('database_port', getenv("OPENSHIFT_MYSQL_DB_PORT"));
    $container->setParameter('database_name', getenv("OPENSHIFT_APP_NAME"));
    $container->setParameter('database_user', getenv("OPENSHIFT_MYSQL_DB_USERNAME"));
    $container->setParameter('database_password', getenv("OPENSHIFT_MYSQL_DB_PASSWORD"));
}

if (getenv('MINICC_MAILER_PASSWORD')) {
    $container->setParameter('mailer_password', getenv('MINICC_MAILER_PASSWORD'));
}

if (getenv('MINISHIFT_SERVER_NAME')) {
	$container->setParameter('database_name', 'minicc');
	$container->setParameter('database_user', getenv("MINISHIFT_MYSQL_DB_USERNAME"));
	$container->setParameter('database_password', getenv("MINISHIFT_MYSQL_DB_PASSWORD"));
}
