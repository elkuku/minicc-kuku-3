#!/usr/bin/env bash

CONSOLE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/console

${CONSOLE} cache:clear --env=prod
