#!/usr/bin/env bash

ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CONSOLE=${ROOT}/console

${CONSOLE} doctrine:database:drop --force --ifExist
${CONSOLE} doctrine:database:create
${CONSOLE} doctrine:schema:update --force
${CONSOLE} doctrine:fixtures:load

#${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../tests/test-data.sql)"

${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../temp/dump-user.sql)"
${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../temp/dump-stores.sql)"
${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../temp/dump-transactions1.sql)"
${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../temp/dump-contracts.sql)"
${CONSOLE} doctrine:query:sql "$(cat ${ROOT}/../temp/dump-deposit.sql)"
