INSERT INTO `user` (`id`, `gender_id`, `state_id`, `email`, `name`, `role`, `password`, `inq_ci`, `inq_ruc`, `telefono`,
                    `telefono2`, `direccion`) VALUES
  (2, 1, 1, 'user1@a.b', 'User 1', 'ROLE_USER', '$2y$13$MKm4s/Q/qxySC1z4oRRHYureVhf1R/ETQ5LZJdninq0pIXwbshRCO', '123', '', '', '', ''),
  (3, 1, 1, 'user2@a.b', 'User 2', 'ROLE_USER', '$2y$13$Gfbu1gPT6250jsbXMrlByuzbH9xo5O0b2zjUp0GhGkpE4wPE.s6C6', '123', '', '', '', ''),
  (4, 1, 1, 'user3@a.b', 'User 3', 'ROLE_USER', '$2y$13$ehxAWzZ2th4d5z5Z0qf87ut9fkNoStt.FyLyrSuSuvKmfTQIqXOW2', '123', '', '', '', ''),
  (5, 1, 1, 'user4@a.b', 'User 4', 'ROLE_USER', '$2y$13$vgTKvCmD4SHUywddkrfX2.cD4bJk8O2o4RJPImY08TXbFJHVATx5W', '123', '', '', '', '');

INSERT INTO `store` (`id`, `user_id`, `destination`, `val_alq`, `cnt_lanfort`, `cnt_neon`, `cnt_switch`, `cnt_toma`,
                     `cnt_ventana`, `cnt_llaves`, `cnt_med_agua`, `cnt_med_elec`, `med_electrico`, `med_agua`) VALUES
(1, 2, 'Local Uno', 123, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 3, 'Local Dos', 234, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 4, 'Local Tres', 522, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 5, 'Local Cuatro', 184, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 3, 'Local Cinco', 251, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
