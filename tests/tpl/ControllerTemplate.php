<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class CLASSNAME
 * @package Tests\AppBundle\Controller
 */
class CLASSNAME extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            #URLS#
        ];
    }
}
