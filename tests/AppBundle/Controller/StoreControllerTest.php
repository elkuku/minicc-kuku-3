<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class StoreControllerTest
 * @package Tests\AppBundle\Controller
 */
class StoreControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/stores'],
['/store-add'],
['/store-edit/1'],
['/store/1'],
['/store-transaction-pdf/1/{year}']
        ];
    }
}
