<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class DepositControllerTest
 * @package Tests\AppBundle\Controller
 */
class DepositControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/deposits'],
['/upload-csv'],
['/lookup-depo']
        ];
    }
}
