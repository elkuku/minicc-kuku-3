<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class AdminControllerTest
 * @package Tests\AppBundle\Controller
 */
class AdminControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/cobrar'],
['/pay-day'],
['/planillas'],
['/pagos-por-ano'],
['/admin-tasks'],
['/test-mail'],
['/backup']
        ];
    }
}
