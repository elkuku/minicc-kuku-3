<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class TransactionControllerTest
 * @package Tests\AppBundle\Controller
 */
class TransactionControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/transaction-delete/1'],
['/transaction-edit/1'],
['/transaction-rawlist']
        ];
    }
}
