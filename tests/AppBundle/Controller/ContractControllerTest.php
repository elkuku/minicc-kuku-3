<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class ContractControllerTest
 * @package Tests\AppBundle\Controller
 */
class ContractControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/contracts'],
['/contracts-new'],
['/contract/1'],
['/contracts-template'],
['/contract-generate/1']
        ];
    }
}
