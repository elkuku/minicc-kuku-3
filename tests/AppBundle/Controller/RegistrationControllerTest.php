<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class RegistrationControllerTest
 * @package Tests\AppBundle\Controller
 */
class RegistrationControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/register']
        ];
    }
}
