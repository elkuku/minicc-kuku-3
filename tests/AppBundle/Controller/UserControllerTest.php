<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class UserControllerTest
 * @package Tests\AppBundle\Controller
 */
class UserControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/users'],
['/users-pdf'],
['/user-edit/1'],
['/users-ruclist']
        ];
    }
}
