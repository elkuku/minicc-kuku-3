<?php

namespace Tests\AppBundle\Controller;

use Tests\AbstractController;

/**
 * Class PaymentMethodControllerTest
 * @package Tests\AppBundle\Controller
 */
class PaymentMethodControllerTest extends AbstractController
{
    /**
     * @dataProvider urlProvider
     *
     * @param string $url
     */
    public function testPageLoad($url)
    {
        $this->checkPage($url);
    }

    /**
     * @return array
     */
    public function urlProvider()
    {
        return [
            ['/payment-methods'],
['/payment-methods-new'],
['/payment-methods-edit/1'],
['/payment-methods-delete/1']
        ];
    }
}
