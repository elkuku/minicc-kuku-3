<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Created by PhpStorm.
 * User: elkuku
 * Date: 30.03.17
 * Time: 22:00
 */
class AbstractController extends WebTestCase
{
    protected $urlMap = [
        '/' => 'public',
        '/about' => 'public',
        '/contact' => 'public',
        '/login' => 'public',
        '/logout' => 'localhost',
    ];

    protected function checkPage($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $response = $client->getResponse();

        if (array_key_exists($url, $this->urlMap)) {
            switch ($this->urlMap[$url]) {
                case 'public':
                    $this->assertTrue($client->getResponse()->isOk());
                    break;

                case 'localhost':
                    $this->assertEquals('http://localhost/', $response->getTargetUrl());
                    break;

                default:
                    $this->assertEquals('/login', $response->getTargetUrl());
                    $this->assertTrue($client->getResponse()->isRedirection());
                    break;
            }
        } else {
            $this->assertEquals('/login', $response->getTargetUrl());
            $this->assertTrue($client->getResponse()->isRedirection());

        }
    }
}
