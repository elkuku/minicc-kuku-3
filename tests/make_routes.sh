#!/usr/bin/env bash

CONSOLE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../bin/console

${CONSOLE}  debug:router --format=json --show-controllers > routes.json
