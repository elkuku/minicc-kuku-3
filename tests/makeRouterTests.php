<?php
/**
 * Created by PhpStorm.
 * User: elkuku
 * Date: 30.03.17
 * Time: 12:22
 */

$data = json_decode(file_get_contents(__DIR__.'/routes.json'));

$routes = [];

foreach ($data as $name => $item) {
    if (0 === strpos($name, '_')) {
        continue;
    }

    $parts = explode(':', $item->defaults->_controller);

    if (3 == count($parts)) {
        $routes[$parts[0]][$parts[1]][$parts[2]] = $item;
    } else {
        throw new Exception('boa');
    }


}

$tpl = file_get_contents(__DIR__.'/tpl/ControllerTemplate.php');

foreach ($routes as $bundle => $controllers) {
    foreach ($controllers as $controller => $methods) {
        $urls = [];
        foreach ($methods as $name => $data) {
            $urls[] = str_replace('{id}', '1', $data->path);
        }
        $fileName = __DIR__.sprintf('/%s/Controller/%sControllerTest.php', $bundle, $controller);

        $contents = str_replace('CLASSNAME', $controller.'ControllerTest', $tpl);
        $contents = str_replace('#URLS#', "['".implode("'],\n['", $urls)."']", $contents);

        file_put_contents($fileName, $contents);

    }
}
