<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserState;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadTransactionTypeData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserStateData implements FixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $names = ['Activo', 'Inactivo'];

        foreach ($names as $name) {
            $userState = new UserState();

            $userState->setName($name);

            $manager->persist($userState);
        }

        $manager->flush();
    }
}
