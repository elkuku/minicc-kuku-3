<?php
/**
 * Created by PhpStorm.
 * User: elkuku
 * Date: 22.03.17
 * Time: 00:09
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadPaymentMethodData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user->setName('admin')
            ->setEmail('admin@a.b')
            ->setPlainPassword('test')
            ->setRole('ROLE_ADMIN')
            ->setPassword(
                $this->container->get('security.password_encoder')
                    ->encodePassword($user, $user->getPlainPassword())
            )
        ;

        $manager->persist($user);
        $manager->flush();
    }
}
