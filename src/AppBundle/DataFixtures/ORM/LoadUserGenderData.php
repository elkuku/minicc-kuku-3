<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserGender;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadTransactionTypeData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserGenderData implements FixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $names = ['Sr', 'Sra'];

        foreach ($names as $name) {
            $userGender = new UserGender();

            $userGender->setName($name);

            $manager->persist($userGender);
        }

        $manager->flush();
    }
}
