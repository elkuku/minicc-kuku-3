<?php
namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFullType
 * @package AppBundle\Form
 */
class UserFullType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'state',
                EntityType::class,
                array(
                    'class'        => 'AppBundle:UserState',
                    'choice_label' => 'name',
                )
            )
            ->add(
                'gender',
                EntityType::class,
                array(
                    'class'        => 'AppBundle:UserGender',
                    'choice_label' => 'name',
                )
            )
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('inqCi')
            ->add('inqRuc', null, ['required' => false])
            ->add('telefono', null, ['required' => false])
            ->add('telefono2', null, ['required' => false])
            ->add('direccion', null, ['required' => false])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Password'],
                'second_options' => ['label' => 'Confirm Password'],
                'required'     => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\User',
        ]);
    }
}
