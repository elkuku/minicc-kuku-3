<?php

namespace AppBundle\Helper\CsvParser;

/**
 * Class CsvObject
 * @package AppBundle\Helper\CsvParser
 */
class CsvObject
{
    /**
     * @var array
     */
    public $headVars = [];

    /**
     * @var array
     */
    public $lines = [];
}
