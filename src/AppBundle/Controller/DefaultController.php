<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Transaction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ListController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="welcome")
     * @return Response
     */
    public function showAction()
    {
        $user = $this->getUser();

        if ($user) {
            $stores = $user->getStores();
            $saldos = $this->getDoctrine()->getRepository(Transaction::class)->getSaldos();
        } else {
            $saldos = null;
            $stores = null;
        }

        return $this->render(
            'default/index.html.twig',
            [
                'stores' => $stores,
                'saldos' => $saldos,
            ]
        );
    }

    /**
     * @Route("/about", name="about")
     * @return Response
     */
    public function aboutAction()
    {
        return $this->render('default/about.html.twig', ['user' => $this->getUser()]);
    }

    /**
     * @Route("/contact", name="contact")
     * @return Response
     */
    public function contactAction()
    {
        return $this->render('default/contact.html.twig');
    }
}
